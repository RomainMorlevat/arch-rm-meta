FROM docker.io/library/archlinux:base-devel

LABEL com.github.containers.toolbox="true" \
      name="arch-toolbox" \
      version="base-devel" \
      usage="This image is meant to be used with the toolbox command" \
      summary="Custom base image for creating Arch Linux toolbox containers" \
      maintainer="Romain Morlevat <dev@romainmorlevat.com>" \
      inspired_by="toolbx-images from Morten Linderud"

RUN pacman -Syu --needed --noconfirm

# Install extra packages
RUN pacman -Syu --noconfirm bat diffutils eza \
  flatpak-xdg-utils fd fzf git github-cli git-delta gnupg keyutils \
  libyaml lsof man-db man-pages mat2 neovim openssh openssl-1.1 pigz \
  postgresql-libs procps-ng ripgrep rsync starship stow tmux tree \
  unzip vte-common wget xorg-xauth zip zoxide\
  zsh zsh-history-substring-search \
  zsh-syntax-highlighting

# Clean up cache
RUN pacman -Scc --noconfirm

# Enable sudo permission for wheel users
RUN echo "%wheel ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/toolbox
