# I get fonts directly from the repo instead of using an Arch/AUR package
# because I need it also for Silverblue.
cd ~
wget https://github.com/ryanoasis/nerd-fonts/releases/latest/download/Inconsolata.tar.xz
mkdir -p ~/.local/share/fonts
tar -C ~/.local/share/fonts -xvf Inconsolata.tar.xz InconsolataNerdFontMono-Regular.ttf
rm -rf Inconsolata.tar.xz 

cd ~
wget https://github.com/ryanoasis/nerd-fonts/releases/latest/download/Iosevka.tar.xz 
mkdir -p ~/.local/share/fonts
tar -C ~/.local/share/fonts -xvf Iosevka.tar.xz IosevkaNerdFont-Regular.ttf
rm -rf Iosevka.tar.xz 

