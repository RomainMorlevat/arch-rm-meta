Install the following extensions:
- [blur-my-shell](https://github.com/aunetx/blur-my-shell);
- [space-bar](https://github.com/christopher-l/space-bar);
- [tilingshell](https://github.com/domferr/tilingshell);
- [GPaste](https://github.com/Keruspe/GPaste);
- [gnome-shell-extension-appindicator](https://github.com/ubuntu/gnome-shell-extension-appindicator).
