# no dynamic and 4 workspaces
gsettings set org.gnome.mutter dynamic-workspaces false
gsettings set org.gnome.desktop.wm.preferences num-workspaces 4

# Windows
gsettings set org.gnome.desktop.wm.keybindings switch-windows "['<Alt>Tab']"

# Reset defaults keybindings for switching apps (weirdly the following for workspaces doesn't override them)
gsettings reset org.gnome.shell.keybindings switch-to-application-1
gsettings reset org.gnome.shell.keybindings switch-to-application-2
gsettings reset org.gnome.shell.keybindings switch-to-application-3
gsettings reset org.gnome.shell.keybindings switch-to-application-4

# Workspaces
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-1 "['<Super>1']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-2 "['<Super>2']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-3 "['<Super>3']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-4 "['<Super>4']"
