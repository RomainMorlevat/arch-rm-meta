# Arch Linux install helpers

This repo is meant to provide me a set of tools to setup a machine with [Arch Linux](https://archlinux.org/), be it a [toolbox](https://containertoolbx.org/) ran on [Silverblue](https://docs.fedoraproject.org/en-US/fedora-silverblue/) or a bare metal Arch insall.

## To do

- [ ] Set packages installation script;
- [ ] Set yay installation script;
- [ ] Set AUR packages installation script;
- [ ] Set flatpak applications installation script;
- [x] Set fonts;
- [x] Set Gnome settings script;
- [ ] Set theme;

On Arch Linux:

- [ ] Set postgresql in a podman container;
- [ ] Set redis in a podman container;

On Silverblue:

- [ ] Set postgresql in a podman container on host;
- [ ] Set redis in a podman container on host;

## Containerfile

This is the file used by [toolbox](https://containertoolbx.org/) (podman underneath) to build the container.
Place this file into `~/containers/archlinux/` for example.

## Flatpak

### Create a list of installed flatpak
To create a list of installed flatpak, run: `flatpak list --app --columns application:f > flatpak_list`.

### To install a list from the flatpak_list file

Run `cat flatpak_list | xargs flatpak install`.

## PKGBUILD

My first idea was to create a meta package to handle installation of software I need, but I don't have time to dig into that for now. See commit [3be8112e](https://gitlab.com/RomainMorlevat/arch-rm/-/commit/3be8112e50a0bad91f8ce10848e8ac2353d650e1) for the draft.
